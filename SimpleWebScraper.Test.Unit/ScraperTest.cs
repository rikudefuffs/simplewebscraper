﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleWebScraper.Workers;
using SimpleWebScraper.Data;
using SimpleWebScraper.Builders;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace SimpleWebScraper.Test.Unit
{
    [TestClass]
    public class ScraperTest
    {
        readonly Scraper scraper = new Scraper();

        [TestMethod]
        public void FindCollectionWithNoParts()
        {
            string content = "bla bla data <a href=\"https://rome.craigslist.org/apa/d/large-studio-in-central-rome-for-long/6843767731.html\" data-id=\"6843767731\" class=\"result-title hdrlnk\">Large studio in Central ROME for LONG LET</a>";
            ScrapeCriteria scrapeCriteria = new ScrapeCriteriaBuilder().WithData(content)
                                            .WithRegex(@"<a href=\""(.*?)\"" data-id=\""(.*?)\"" class=\""result-title hdrlnk\"">(.*?)</a>")
                                            .WithRegexOptions(RegexOptions.ExplicitCapture).Build();

            List<string> scrapedElements = scraper.Scrape(scrapeCriteria);
            Assert.IsTrue(scrapedElements.Count == 1);
            Assert.IsTrue(scrapedElements[0].Equals("<a href=\"https://rome.craigslist.org/apa/d/large-studio-in-central-rome-for-long/6843767731.html\" data-id=\"6843767731\" class=\"result-title hdrlnk\">Large studio in Central ROME for LONG LET</a>"));
        }

        [TestMethod]
        public void FindCollectionWithTwoParts()
        {
            string content = "bla bla data <a href=\"https://rome.craigslist.org/apa/d/large-studio-in-central-rome-for-long/6843767731.html\" data-id=\"6843767731\" class=\"result-title hdrlnk\">Large studio in Central ROME for LONG LET</a>";
            ScrapeCriteriaPart titleGrabber = new ScrapeCriteriaPartBuilder().WithRegex(@">(.*?)</a>")
                                                                                    .WithRegexOptions(RegexOptions.Singleline).Build();
            ScrapeCriteriaPart linkGrabber = new ScrapeCriteriaPartBuilder().WithRegex(@"href=\""(.*?)\""")
                                                                            .WithRegexOptions(RegexOptions.Singleline).Build();
            ScrapeCriteria scrapeCriteria = new ScrapeCriteriaBuilder().WithData(content)
                                            .WithRegex(@"<a href=\""(.*?)\"" data-id=\""(.*?)\"" class=\""result-title hdrlnk\"">(.*?)</a>")
                                            .WithPart(titleGrabber)
                                            .WithPart(linkGrabber)
                                            .WithRegexOptions(RegexOptions.ExplicitCapture).Build();

            List<string> scrapedElements = scraper.Scrape(scrapeCriteria);
            Assert.IsTrue(scrapedElements.Count == 2);
            Assert.IsTrue(scrapedElements[0].Equals("Large studio in Central ROME for LONG LET"));
            Assert.IsTrue(scrapedElements[1].Equals("https://rome.craigslist.org/apa/d/large-studio-in-central-rome-for-long/6843767731.html"));
        }


    }
}
