﻿using SimpleWebScraper.Builders;
using SimpleWebScraper.Data;
using SimpleWebScraper.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleWebScraper
{
    class Program
    {
        const string METHOD = "search";

        static void Main(string[] args)
        {
            try
            {
                //ScrapeCraigsList();
                ScrapeRoll20();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.Read();
        }

        static void ScrapeCraigsList()
        {
            // https://rome.craigslist.org/d/app.ti-alloggi-in-affitto/search/apa
            Console.Write("Please enter the city to scrape: ");
            string craigsListcity = Console.ReadLine() ?? string.Empty; // "rome"; //Console.ReadLine() ?? string.Empty;
            Console.Write("Please enter the CraigsList category to scrape: ");
            string craigsListCategory = Console.ReadLine() ?? string.Empty; //"app.ti-alloggi-in-affitto";
            using (WebClient client = new WebClient())
            {
                string webAddress = string.Format($"https://{craigsListcity.Replace(" ", string.Empty)}.craigslist.org/d/{craigsListCategory}/{METHOD}/");
                string content = client.DownloadString(webAddress);

                //element: <a href="https://rome.craigslist.org/apa/d/large-studio-in-central-rome-for-long/6843767731.html" data-id="6843767731" class="result-title hdrlnk">Large studio in Central ROME for LONG LET</a>

                ScrapeCriteriaPart titleGrabber = new ScrapeCriteriaPartBuilder().WithRegex(@">(.*?)</a>")
                                                                                .WithRegexOptions(RegexOptions.Singleline).Build();
                ScrapeCriteriaPart linkGrabber = new ScrapeCriteriaPartBuilder().WithRegex(@"href=\""(.*?)\""")
                                                                                .WithRegexOptions(RegexOptions.Singleline).Build();

                ScrapeCriteria scrapeCriteria = new ScrapeCriteriaBuilder()
                    .WithData(content)
                    .WithRegex(@"<a href=\""(.*?)\"" data-id=\""(.*?)\"" class=\""result-title hdrlnk\"">(.*?)</a>")
                    .WithRegexOptions(RegexOptions.ExplicitCapture)
                    .WithPart(titleGrabber)
                    .WithPart(linkGrabber)
                    .Build();

                Scraper scraper = new Scraper();
                List<string> scrapedElements = scraper.Scrape(scrapeCriteria);
                if (scrapedElements.Any())
                {
                    foreach (var scrapedElement in scrapedElements)
                    {
                        Console.WriteLine(scrapedElement);
                    }
                }
                else
                {
                    Console.WriteLine("NO matches :C");
                }
            }
        }
        static void ScrapeRoll20()
        {
            string forumPrefix = "https://app.roll20.net/forum";
            using (WebClient client = new WebClient())
            {
                string webAddress = "https://app.roll20.net/forum/";
                string content = client.DownloadString(webAddress);

                //element: <a href="/forum/post/7308064/roll20-marketplace-upgrade-and-downtime-on-march-26-at-1-6-am-pdt">Roll20 Marketplace Upgrade &amp; Downtime on March 26 @ 1-6 A.M. PDT</a>

                ScrapeCriteriaPart titleGrabber = new ScrapeCriteriaPartBuilder().WithRegex(@">(.*?)</a>")
                                                                                .WithRegexOptions(RegexOptions.Singleline).Build();
                ScrapeCriteriaPart linkGrabber = new ScrapeCriteriaPartBuilder().WithRegex(@"href='(.*?)'")
                                                                                .WithRegexOptions(RegexOptions.Singleline).Build();
                ScrapeCriteria scrapeCriteria = new ScrapeCriteriaBuilder()
                    .WithData(content)
                    .WithRegex(@"<a href='/forum/post/(.*?)'>(.*?)</a>")
                    .WithRegexOptions(RegexOptions.ExplicitCapture)
                    .WithPart(titleGrabber)
                    .WithPart(linkGrabber)
                    .Build();

                Scraper scraper = new Scraper();
                List<string> scrapedElements = scraper.Scrape(scrapeCriteria);
                int i = 0;
                if (scrapedElements.Any())
                {
                    foreach (var scrapedElement in scrapedElements)
                    {
                        if (i % 2 == 0)
                        {
                            Console.WriteLine("------------");
                            Console.WriteLine(scrapedElement);
                        }
                        else
                        {
                            Console.WriteLine(string.Format("{0}{1}", forumPrefix, scrapedElement));
                        }
                        i++;
                    }
                }
                else
                {
                    Console.WriteLine("NO matches :C");
                }
            }
        }
    }
}
