﻿using SimpleWebScraper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleWebScraper.Builders
{
    class ScrapeCriteriaBuilder
    {
        string data;
        string regex;
        RegexOptions regexOption;
        List<ScrapeCriteriaPart> parts;

        public ScrapeCriteriaBuilder()
        {
            SetDefaults(); //Builder Pattern
        }

        /// <summary>
        /// Main method: sets default values
        /// </summary>
        void SetDefaults()
        {
            data = string.Empty;
            regex = string.Empty;
            regexOption = RegexOptions.None;
            parts = new List<ScrapeCriteriaPart>();
        }

        public ScrapeCriteriaBuilder WithData(string data)
        {
            this.data = data;
            return this;
        }

        public ScrapeCriteriaBuilder WithRegex(string regex)
        {
            this.regex = regex;
            return this;
        }

        public ScrapeCriteriaBuilder WithRegexOptions(RegexOptions regexOption)
        {
            this.regexOption = regexOption;
            return this;
        }

        public ScrapeCriteriaBuilder WithPart(ScrapeCriteriaPart part)
        {
            parts.Add(part);
            return this;
        }

        public ScrapeCriteria Build()
        {
            ScrapeCriteria scrapeCriteria = new ScrapeCriteria();
            scrapeCriteria.data = data;
            scrapeCriteria.regex = regex;
            scrapeCriteria.regexOption = regexOption;
            scrapeCriteria.parts = parts;
            return scrapeCriteria;
        }
    }
}
