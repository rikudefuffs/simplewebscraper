﻿using SimpleWebScraper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleWebScraper.Builders
{
    class ScrapeCriteriaPartBuilder
    {
        string regex;
        RegexOptions regexOption;

        public ScrapeCriteriaPartBuilder()
        {
            SetDefaults(); //Builder Pattern
        }

        /// <summary>
        /// Main method: sets default values
        /// </summary>
        void SetDefaults()
        {
            regex = string.Empty;
            regexOption = RegexOptions.None;
        }

        public ScrapeCriteriaPartBuilder WithRegex(string regex)
        {
            this.regex = regex;
            return this;
        }

        public ScrapeCriteriaPartBuilder WithRegexOptions(RegexOptions regexOption)
        {
            this.regexOption = regexOption;
            return this;
        }

        public ScrapeCriteriaPart Build()
        {
            ScrapeCriteriaPart scrapeCriteriaPart = new ScrapeCriteriaPart();
            scrapeCriteriaPart.regex = regex;
            scrapeCriteriaPart.regexOption = regexOption;
            return scrapeCriteriaPart;
        }
    }
}
