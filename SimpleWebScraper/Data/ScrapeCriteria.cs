﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleWebScraper.Data
{
    /// <summary>
    /// Tells how to scrape a page
    /// </summary>
    class ScrapeCriteria
    {
        /// <summary>
        /// The data you want to scrape
        /// </summary>
        public string data { get; set; }

        /// <summary>
        /// How you want to scrape the data
        /// </summary>
        public string regex { get; set; }
        public RegexOptions regexOption { get; set; }

        /// <summary>
        /// How granular do we want to go with our scraping?
        /// </summary>
        public List<ScrapeCriteriaPart> parts { get; set; }
        public ScrapeCriteria()
        {
            parts = new List<ScrapeCriteriaPart>();
        }
    }
}
