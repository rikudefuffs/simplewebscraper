﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleWebScraper.Data
{
    class ScrapeCriteriaPart
    {
        public string regex { get; set; }
        public RegexOptions regexOption { get; set; }
    }
}
