﻿using SimpleWebScraper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleWebScraper.Workers
{
    class Scraper
    {
        public List<string> Scrape(ScrapeCriteria scrapeCriteria)
        {
            List<string> scrapedElements = new List<string>();
            MatchCollection matches = Regex.Matches(scrapeCriteria.data, scrapeCriteria.regex, scrapeCriteria.regexOption);
            foreach (Match match in matches)
            {
                if (!scrapeCriteria.parts.Any())
                {
                    //match.Groups[0].Value --> our element (I.E: a full <div>)
                    scrapedElements.Add(match.Groups[0].Value);
                }
                else
                {
                    //let's go more granular and take a single part of the element (I.E: ID)
                    foreach (ScrapeCriteriaPart part in scrapeCriteria.parts)
                    {
                        //this time you use a Match isntead of a MatchCollection because you're supposed to find only 1 copy of an attribute in the element
                        Match matchedPart = Regex.Match(match.Groups[0].Value, part.regex, part.regexOption);
                        if (matchedPart.Success)
                        {
                            //Console.WriteLine(string.Format("Part 0 is {0} | Part 1 is {1} ", matchedPart.Groups[0].Value, matchedPart.Groups[1].Value));
                            scrapedElements.Add(matchedPart.Groups[1].Value);
                        }
                    }
                }
            }
            return scrapedElements;
        }
    }
}
